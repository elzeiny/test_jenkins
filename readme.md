# Dextor Pipeline through Jenkins Notes
Submodules in Python can be setup through an __init__.py file, or through sys path imports. For example:

 > import sys
 > sys.path.insert(0, "./submodule")
 > 
 > from submodule_f import *

However, Dextor has its own system for submodule imports.

1. Comment out the sys path imports, and/or add __init__.py in the .gitignore file

2. Create a json object on the root directory, i.e: dextor_deploy.json

3. Follow the JSON structure included in this repo

4. Upload to bitbucket, copy the SSH git clone path

5. Open Jenkins, find "Project Deploy Applications to Dextor", Then "Build with Parameters"

6. Specify environment, paste the git clone path, and the deployment descriptor should be the afforemented JSON file

7. Click Build. Try again if there is a failure, otherwise go to dextor

8. If the build was a sucess and the project isn't showing on Dextor, then you need to ask someone with the permissions to add the project to the Test page

9. Run the project, everything should be ok.


## Notes on importing
 * Dextor stores everything in a flat structure, even if the repo has directories
 * If a module (submodule_f) requires another module (submodule_f_util), then place both modules under the "module" section of the json, and add both as dependancies each time you want to use submodule_f.